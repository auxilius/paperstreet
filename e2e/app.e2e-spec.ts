import { PaperstreetPage } from './app.po';

describe('paperstreet App', function() {
  let page: PaperstreetPage;

  beforeEach(() => {
    page = new PaperstreetPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
