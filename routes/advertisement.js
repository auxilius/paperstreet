var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Advertisement = require('../models/advertisement.js');

/* GET Search Advertisements */
/* This service accepts below requests parameter */
/*    keywords - comma separated keyword to search  */
/*    limit - maximum number of results to return */

router.get('/', function (req, res, next) {

  var keywords = req.query.keywords;
  var recordCount = parseInt(req.query.limit);

  var query = Advertisement.find();


  if (keywords) {
    query.limit(recordCount);

    var orCondition = [];

    var keywordList = keywords.split(",");


    for (var i = 0; i < keywordList.length; i++) {
      orCondition.push({keywords: keywordList[i].trim()});
    }

    Advertisement.find({$or: orCondition}, function (err, result) {
      if (err) return next(err);

      var searchResult = {
        "resultCount": result.length,
        "resultItems": result
      }
      res.json(searchResult);

    }).limit(recordCount);

  } else {

    Advertisement.find(function (err, result) {
      if (err) return next(err);

      var searchResult = {
        "resultCount": result.length,
        "resultItems": result
      }
      res.json(searchResult);
    }).limit(recordCount);
  }
});

/* GET SINGLE Advertisement BY ID */
router.get('/:id', function (req, res, next) {
  Advertisement.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* SAVE Advertisement use for adding new record*/
router.post('/', function (req, res, next) {
  Advertisement.create(req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* UPDATE Advertisement use to update existing record */
router.put('/:id', function (req, res, next) {
  Advertisement.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE Advertisement accepts ID to match and delete*/
router.delete('/:id', function (req, res, next) {
  Advertisement.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;
