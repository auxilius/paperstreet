// Insert sample data into Advertisement collection

var path = require("path"),
  Advertisement = require(path.join(__dirname, "..", "models", "advertisement.js")),
  mongoose_uri = process.env.MONGOOSE_URI || "localhost/paperstreet";

// var Advertisement = require('../models/advertisement.js');

console.log("Creating a sample data in Mongo");

var mongoose = require('mongoose');
mongoose.set('debug', true);

mongoose.connect('mongodb://localhost/paperstreet', { promiseLibrary: require('bluebird') })
  .then(() =>  console.log('connection successful'))
.catch((err) => console.error(err));


mongoose.connection.on('error', function () {
  console.log('Mongoose connection error', arguments);
});

mongoose.connection.once('open', function callback() {
  console.log("Mongoose connected to the database");

 var ad1 = new Advertisement();

  ad1.url = "https://imaging.nikon.com/lineup/dslr/d600/img/sample01/img_01.png";
  ad1.width = 200;
  ad1.height = 100;
  ad1.keywords = ["vacation", "landscape"];

  ad1.save(function (err) {
    if (err) {
      console.log(err);
    } else {
      console.log("Inserted : " + ad1.url);
    }
  });


  var ad2 = new Advertisement();

  ad2.url = "https://imaging.nikon.com/lineup/dslr/d600/img/sample01/img_02.png";
  ad2.width = 100;
  ad2.height = 2200;
  ad2.keywords = ["portrait", "pink", "lady"];

  ad2.save(function (err) {
    if (err) {
      console.log(err);
    } else {
      console.log("Inserted : " + ad2.url);
    }
  });

  var ad3 = new Advertisement();

  ad3.url = "https://imaging.nikon.com/lineup/dslr/d600/img/sample01/img_03.png";
  ad3.width = 200;
  ad3.height = 100;
  ad3.keywords = ["man", "shore", "life"];

  ad3.save(function (err) {
    if (err) {
      console.log(err);
    } else {
      console.log("Inserted : " + ad3.url);
    }
  });


  var ad4 = new Advertisement();

  ad4.url = "https://imaging.nikon.com/lineup/dslr/d600/img/sample01/img_04.png";
  ad4.width = 200;
  ad4.height = 100;
  ad4.keywords = ["bird", "nature", "wildlife", "landscape"];

  ad4.save(function (err) {
    if (err) {
      console.log(err);
    } else {
      console.log("Inserted : " + ad4.url);
    }
  });


  var ad5 = new Advertisement();

  ad5.url = "https://imaging.nikon.com/lineup/dslr/d600/img/sample01/img_05.png";
  ad5.width = 200;
  ad5.height = 100;
  ad5.keywords = ["vacation", "landscape"];

  ad5.save(function (err) {
    if (err) {
      console.log(err);
    } else {
      //console.log(ad5);
      console.log("Inserted : " + ad5.url);
    }
  });

  var ad6 = new Advertisement();

  ad6.url = "https://imaging.nikon.com/lineup/dslr/d600/img/sample01/img_06.png";
  ad6.width = 200;
  ad6.height = 100;
  ad6.keywords = [ "landscape", "nature"];

  ad6.save(function (err) {
    if (err) {
      console.log(err);
    } else {
      console.log("Inserted : " + ad6.url);
    }
  });

});
