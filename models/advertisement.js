var mongoose = require('mongoose');

// This is the data model for Advertisement collection
var AdvertisementSchema = new mongoose.Schema({
  url: String,
  width: Number,
  height: Number,
  keywords: [],
  modified_date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Advertisent', AdvertisementSchema);
