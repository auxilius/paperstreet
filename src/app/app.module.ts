import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AdvertisementComponent } from './advertisement/advertisement.component';
import { AdvertisementDetailComponent } from './advertisement-detail/advertisement-detail.component';
import { AdvertisementCreateComponent } from './advertisement-create/advertisement-create.component';
import { AdvertisementEditComponent } from './advertisement-edit/advertisement-edit.component';

import { RouterModule, Routes } from '@angular/router';
import {HttpClientModule} from "@angular/common/http";

const appRoutes: Routes = [
  {
    path: 'advertisement-list',
    component: AdvertisementComponent,
    data: { title: 'Advertisement List' }
  },
  {
    path: 'advertisement-details/:id',
    component: AdvertisementDetailComponent,
    data: { title: 'Advertisement Details' }
  },
  {
    path: 'advertisement-create',
    component: AdvertisementCreateComponent,
    data: { title: 'Create Advertisement' }
  },
  {
    path: 'advertisement-edit/:id',
    component: AdvertisementEditComponent,
    data: { title: 'Edit Advertisement' }
  },
  { path: '',
    redirectTo: '/advertisement-list',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    AdvertisementComponent,
    AdvertisementDetailComponent,
    AdvertisementCreateComponent,
    AdvertisementEditComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
